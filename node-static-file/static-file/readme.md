> 本项目主要使用Node.js启动静态文件服务器，并且做到容器化

### 构建Docker镜像

- 上传此文件夹到任意含有`Docker`的机器上
- 进入文件夹下：`cd $workdir`
- 执行build命令：`docker build -t express-static:latest .`


### 启动容器

- 宿主机创建文件夹，用来保存文件. `mkdir -p /root/files`
- 创建容器：`docker run -d -p 2333:3000 --name file -v /root/files:/app/public/files express-static:latest`

